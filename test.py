import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf

dow = list(pd.read_csv('./stock/Dataset/^DJI.csv')['Close'])
gs = list(pd.read_csv('./stock/Dataset/^GSPC.csv')['Close'])
ix = list(pd.read_csv('./stock/Dataset/^IXIC.csv')['Close'])

_output = []
for d in dow:
    _output.append([float(d)])
_input = []
for g, i in zip(gs, ix):
    _input.append([float(g),float(i)])

print(_output)
print(len(_output), len(_input))
print(_input)

x = tf.placeholder(tf.float32, shape=[None, 1])
y = tf.placeholder(tf.float32, shape=[None, 1])
w = tf.Variable(tf.random_normal([1, 1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

hypothesis = tf.sigmoid(tf.matmul(x, w)+b)
cost = -tf.reduce_mean(y * tf.log(hypothesis) + (1-y) * tf.log(1-hypothesis))

learning_rate = 0.01

optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
train = optimizer.minimize(cost)

# gradient = tf.reduce_mean((w*x-y)*x)
# decent = w - learning_rate*gradient
# update = w.assign(decent)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

w_vals = []
cost_vals = []

for step in range(1000):
    cost_v, h_v, train_v = sess.run([cost, hypothesis, train], feed_dict={x: [[1], [2], [3]], y: [[100],[200],[300]]})
    # w_vals.append(w_val)
    # cost_vals.append(cost_val)
    # if step % 20 == 0:
    print(step, cost_v, h_v, train_v)

# plt.plot(w_vals, cost_vals)
# plt.show()
# logits = tf.add(tf.matmul(x, w), b)
# print(logits)
# cost = tf.reduce_mean()