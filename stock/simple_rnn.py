"""
* simple rnn. data is not good. just for study
"""

import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
CONSTANT = tf.app.flags
CONSTANT.DEFINE_integer('samples', 1000, 'simulation data samples')
CONSTANT.DEFINE_integer('hidden', 1000, 'hidden layers')
CONSTANT.DEFINE_integer('vec_size', 1000, 'input vector size')
FLAGS = CONSTANT.FLAGS


class RNNExample(object):
    def __init__(self):
        self.sess = tf.Session()

    def run(self):
        self._gen_sim_data()
        print(self.sess.run())
        self.sess.close()

    def _gen_sim_data(self):
        ts_x = tf.constant(list(range(FLAGS.samples)), dtype=tf.float32)
        ts_y = tf.sin(ts_x)

        batch = (FLAGS.samples/FLAGS.hidden, FLAGS.hidden, FLAGS.vec_size)
        self.batch_input = tf.reshape(ts_y[:-1], batch)
        self.batch_label = tf.reshape(ts_y[1:], batch)


def main(_):
    """main func of tf"""
    RNNExample().run()


if __name__ == '__main__':
    tf.app.run()