# Solution is available in the other "sandbox_solution.py" tab
import pandas as pd
import tensorflow as tf

dow = pd.read_csv('./Dataset/^DJI.csv')
print('dow :', dow)

n_features = 784
n_labels = 3

# Features and Labels
features = tf.placeholder(tf.float32)
labels = tf.placeholder(tf.float32)

# Weights and Biases
w = tf.Variable(tf.truncated_normal((n_features, n_labels)))
b = tf.Variable(tf.zeros(n_labels))

# Linear Function xW + b
logits = tf.add(tf.matmul(input, w), b)

# Training data
train_features, train_labels = None

# with tf.Session() as session:
#     session.run(tf.global_variables_initializer())
#
#     # Softmax
#     prediction = tf.nn.softmax(logits)
#     # Cross entropy
#     # This quantifies how far off the predictions were.
#     # You'll learn more about this in future lessons.
#     cross_entropy = -tf.reduce_sum(labels * tf.log(prediction), reduction_indices=1)
#
#     # Training loss
#     # You'll learn more about this in future lessons.
#     loss = tf.reduce_mean(cross_entropy)
#
#     # Rate at which the weights are changed
#     # You'll learn more about this in future lessons.
#     learning_rate = 0.08
#
#     # Gradient Descent
#     # This is the method used to train the model
#     # You'll learn more about this in future lessons.
#     optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
#
#     # Run optimizer and get loss
#     _, l = session.run(
#         [optimizer, loss],
#         feed_dict={features: train_features, labels: train_labels})
#
# # Print loss
# print('Loss: {}'.format(l))
